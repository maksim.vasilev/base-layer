terraform {
  backend gcs {
    bucket = "kubernetes-platform-demo-develop-tfstate"
    prefix = "terraform/state"
  }
}